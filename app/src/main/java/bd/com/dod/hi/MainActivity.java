package bd.com.dod.hi;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //alif
        final MediaPlayer catSoundMediaPlayer = MediaPlayer.create(this, R.raw.alif);
        final Button playCatMeow = (Button) this.findViewById(R.id.button);
        playCatMeow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catSoundMediaPlayer.start();
            }
        });

        //ba
        final MediaPlayer baMediaPlayer = MediaPlayer.create(this, R.raw.ba);
        final Button basound = (Button) this.findViewById(R.id.ba);
        basound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baMediaPlayer.start();
            }
        });

        //ta
        final MediaPlayer taMediaPlayer = MediaPlayer.create(this, R.raw.ta);
        final Button tasound = (Button) this.findViewById(R.id.ta);
        tasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taMediaPlayer.start();
            }
        });

        //sa
        final MediaPlayer saMediaPlayer = MediaPlayer.create(this, R.raw.sa);
        final Button sasound = (Button) this.findViewById(R.id.sa);
        sasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saMediaPlayer.start();
            }
        });


        //zim
        final MediaPlayer zimMediaPlayer = MediaPlayer.create(this, R.raw.zim);
        final Button zimsound = (Button) this.findViewById(R.id.zim);
        zimsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zimMediaPlayer.start();
            }
        });

        //ha
        final MediaPlayer haMediaPlayer = MediaPlayer.create(this, R.raw.ha);
        final Button hasound = (Button) this.findViewById(R.id.ha);
        hasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                haMediaPlayer.start();
            }
        });

        //kha
        final MediaPlayer khaMediaPlayer = MediaPlayer.create(this, R.raw.kha);
        final Button khasound = (Button) this.findViewById(R.id.kha);
        khasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                khaMediaPlayer.start();
            }
        });

        //dal
        final MediaPlayer dalMediaPlayer = MediaPlayer.create(this, R.raw.dal);
        final Button dalsound = (Button) this.findViewById(R.id.dal);
        dalsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dalMediaPlayer.start();
            }
        });

        //jal
        final MediaPlayer jalMediaPlayer = MediaPlayer.create(this, R.raw.jal);
        final Button jalsound = (Button) this.findViewById(R.id.jal);
        jalsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jalMediaPlayer.start();
            }
        });

        //ra
        final MediaPlayer raMediaPlayer = MediaPlayer.create(this, R.raw.ra);
        final Button rasound = (Button) this.findViewById(R.id.ra);
        rasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                raMediaPlayer.start();
            }
        });

        //ja
        final MediaPlayer jaMediaPlayer = MediaPlayer.create(this, R.raw.ja);
        final Button jasound = (Button) this.findViewById(R.id.ja);
        jasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jaMediaPlayer.start();
            }
        });

        //sin
        final MediaPlayer sinMediaPlayer = MediaPlayer.create(this, R.raw.sin);
        final Button sinsound = (Button) this.findViewById(R.id.sin);
        sinsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sinMediaPlayer.start();
            }
        });

        //shin
        final MediaPlayer shinMediaPlayer = MediaPlayer.create(this, R.raw.shin);
        final Button shinsound = (Button) this.findViewById(R.id.shin);
        shinsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shinMediaPlayer.start();
            }
        });

        //soyad
        final MediaPlayer soyadMediaPlayer = MediaPlayer.create(this, R.raw.soyad);
        final Button soyadsound = (Button) this.findViewById(R.id.soyad);
        soyadsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soyadMediaPlayer.start();
            }
        });

        //doyad
        final MediaPlayer doyadMediaPlayer = MediaPlayer.create(this, R.raw.doyad);
        final Button doyadsound = (Button) this.findViewById(R.id.doyat);
        doyadsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doyadMediaPlayer.start();
            }
        });

        //toya
        final MediaPlayer toyaMediaPlayer = MediaPlayer.create(this, R.raw.toya);
        final Button toyasound = (Button) this.findViewById(R.id.toya);
        toyasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toyaMediaPlayer.start();
            }
        });

        //joya
        final MediaPlayer joyaMediaPlayer = MediaPlayer.create(this, R.raw.joya);
        final Button joyasound = (Button) this.findViewById(R.id.joya);
        joyasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joyaMediaPlayer.start();
            }
        });

        //ain
        final MediaPlayer ainMediaPlayer = MediaPlayer.create(this, R.raw.ain);
        final Button ainsound = (Button) this.findViewById(R.id.ain);
        ainsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ainMediaPlayer.start();
            }
        });

         //goin
        final MediaPlayer goinMediaPlayer = MediaPlayer.create(this, R.raw.goin);
        final Button goinsound = (Button) this.findViewById(R.id.goin);
        goinsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goinMediaPlayer.start();
            }
        });

         //fa
        final MediaPlayer faMediaPlayer = MediaPlayer.create(this, R.raw.fa);
        final Button fasound = (Button) this.findViewById(R.id.fa);
        fasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                faMediaPlayer.start();
            }
        });

         //cof
        final MediaPlayer cofMediaPlayer = MediaPlayer.create(this, R.raw.cof);
        final Button cofsound = (Button) this.findViewById(R.id.cof);
        cofsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cofMediaPlayer.start();
            }
        });

         //caf
        final MediaPlayer cafMediaPlayer = MediaPlayer.create(this, R.raw.caf);
        final Button cafsound = (Button) this.findViewById(R.id.caf);
        cafsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cafMediaPlayer.start();
            }
        });

         //lam
        final MediaPlayer lamMediaPlayer = MediaPlayer.create(this, R.raw.lam);
        final Button lamsound = (Button) this.findViewById(R.id.lam);
        lamsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lamMediaPlayer.start();
            }
        });

         //mim
        final MediaPlayer mimMediaPlayer = MediaPlayer.create(this, R.raw.mim);
        final Button mimsound = (Button) this.findViewById(R.id.mim);
        mimsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mimMediaPlayer.start();
            }
        });

         //nun
        final MediaPlayer nunMediaPlayer = MediaPlayer.create(this, R.raw.nun);
        final Button nunsound = (Button) this.findViewById(R.id.nun);
        nunsound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nunMediaPlayer.start();
            }
        });

         //oya
        final MediaPlayer oyaMediaPlayer = MediaPlayer.create(this, R.raw.oya);
        final Button oyasound = (Button) this.findViewById(R.id.oya);
        oyasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oyaMediaPlayer.start();
            }
        });

         //haha
        final MediaPlayer hahaMediaPlayer = MediaPlayer.create(this, R.raw.haha);
        final Button hahasound = (Button) this.findViewById(R.id.haha);
        hahasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hahaMediaPlayer.start();
            }
        });

         //hamja
        final MediaPlayer hamjaMediaPlayer = MediaPlayer.create(this, R.raw.hamja);
        final Button hamjasound = (Button) this.findViewById(R.id.hamja);
        hamjasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hamjaMediaPlayer.start();
            }
        });


         //yaa
        final MediaPlayer yaaMediaPlayer = MediaPlayer.create(this, R.raw.yaa);
        final Button yaasound = (Button) this.findViewById(R.id.yaa);
        yaasound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yaaMediaPlayer.start();
            }
        });



    }
}
